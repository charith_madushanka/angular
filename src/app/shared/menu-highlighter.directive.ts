import { 
  Directive,
  HostBinding,
  HostListener 
} from '@angular/core';

@Directive({
  selector: '[appMenuHighlighter]'
})
export class MenuHighlighterDirective {

@HostBinding('style.color') color:string ='#ffb7b7'

  constructor() { }

@HostListener('mouseenter') mouseEnter(){
  this.color="#fff2ac"
}

@HostListener('mouseleave') mouseLeave(){
  this.color="#ffb7b7"
}

}
