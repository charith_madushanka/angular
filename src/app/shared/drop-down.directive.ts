import { Directive, HostBinding, HostListener } from "@angular/core";
import _ from "lodash/lodash";

@Directive({
    selector:'[appDropDown]'
})
export class DropDownDirective{
@HostBinding('class.open') open:boolean = false;

@HostListener('mouseenter') mouseEnter(){
    this.open = true;
}

@HostListener('mouseleave') mouseLeave(){
    _.debounce(function(){
        alert();
    },1000)

    this.open = false; 
}

}