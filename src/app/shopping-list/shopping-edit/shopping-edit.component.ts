import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Ingredient } from '../../shared/ingredient.model';

@Component({
  selector: 'app-shopping-edit',
  templateUrl: './shopping-edit.component.html',
  styleUrls: ['./shopping-edit.component.css']
})
export class ShoppingEditComponent implements OnInit {
  @Output('afterAddIngredient') afterAddIngredient = new EventEmitter<Ingredient>();


  constructor() { }

  ngOnInit() {
  }

  onAddIngredient = (ingredientName: string, ingredientAmount: number) => {
    this.afterAddIngredient.emit(new Ingredient(ingredientName, ingredientAmount));
  }

}
