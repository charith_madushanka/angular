import { Component, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html'
})
export class HeaderComponent {
    @Output() navigation = new EventEmitter<string>()


    onSelected = (feature: string) => {
        console.log('onSelected'+feature);
        this.navigation.emit(feature);
    }
}