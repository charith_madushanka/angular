import {
  Recipe
} from './../recipe.model';
import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  AfterViewInit,
  OnChanges,
  AfterViewChecked
} from '@angular/core';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css']
})
export class RecipeListComponent implements OnInit {
  @Output() recipeSelected = new EventEmitter<Recipe>()


  recipes: Recipe[] = [
    new Recipe("Salmon Roasted in Butter",
      "This simple fish dish",
      "https://static01.nyt.com/images/2015/08/14/dining/14ROASTEDSALMON/14ROASTEDSALMON-articleLarge.jpg"),
    new Recipe("Charred Lemon Chicken Piccata",
      "Lightly charring the lemon slices",
      "https://imagesvc.timeincapp.com/v3/mm/image?url=http%3A%2F%2Fcdn-image.myrecipes.com%2Fsites%2Fdefault%2Ffiles%2Fstyles%2F4_3_horizontal_-_1200x900%2Fpublic%2Fcharred-lemon-chicken-piccata-ck.jpg%3Fitok%3D95WcGRLO&w=800&q=85")
  ]
  constructor() { 
  
  }

  ngOnInit() {
   
  }


  onRecipeSelect = (recipe) => {
    this.recipeSelected.emit(recipe);
  }


}
