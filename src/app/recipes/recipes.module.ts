import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RecipesComponent } from './recipes.component';
import { RecipeListComponent } from './recipe-list/recipe-list.component';
import { RecipeDetailComponent } from './recipe-detail/recipe-detail.component';
import { RecipeItemComponent } from './recipe-list/recipe-item/recipe-item.component';
import { DropDownDirective } from '../shared/drop-down.directive';
import { MenuHighlighterDirective } from '../shared/menu-highlighter.directive';


@NgModule({
  imports: [
    CommonModule
    
  ],
  exports: [
    RecipesComponent
  ],
  declarations: [RecipesComponent, RecipeListComponent, RecipeDetailComponent, RecipeItemComponent,DropDownDirective,MenuHighlighterDirective]
})
export class RecipesModule { }
